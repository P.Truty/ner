import itertools
import numpy as np
import pandas as pd
from keras.preprocessing.text import Tokenizer
from keras_preprocessing.sequence import pad_sequences
from keras.layers import Dense, Input, LSTM, Embedding, Bidirectional, Dropout
from keras.models import Model
from keras.initializers.initializers_v2 import Constant
import tensorflow as tf
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

train_ner_df = pd.read_csv('conll2003/train.txt', sep=' ', skip_blank_lines=False)
train_ner_df = train_ner_df.iloc[1:]

# split df to sentences and tags


def split_df_to_sentences_and_tags(df):
    sentences = []
    tags = []
    current_sentences = []
    current_tags = []

    for word, tag in df[['WORD', 'TAG']].values:
        if pd.isnull(word) or pd.isnull(tag):
            sentences.append(current_sentences)
            tags.append(current_tags)
            current_sentences = []
            current_tags = []
            continue

        current_sentences.append(word)
        current_tags.append(tag)
    return sentences, tags


train_sentences, train_tags = split_df_to_sentences_and_tags(train_ner_df)

vocab = set(itertools.chain(*[[w for w in s] for s in train_sentences]))
tags = set(itertools.chain(*[[w for w in s] for s in train_tags]))
sentences_lens = list(map(len, train_sentences))

MAX_LEN = max(sentences_lens)
VOCAB_SIZE = len(vocab)

# words to numbers

words_tokenizer = Tokenizer(num_words=VOCAB_SIZE, filters=[], oov_token='__UNKNOWN__')
words_tokenizer.fit_on_texts(map(lambda s: ' '.join(s), train_sentences))

word_index = words_tokenizer.word_index
word_index['__PADDING__'] = 0
index_word = {i: w for w, i in word_index.items()}

train_sequences = words_tokenizer.texts_to_sequences(map(lambda s: ' '.join(s), train_sentences))
train_sequences_padded = pad_sequences(train_sequences, maxlen=MAX_LEN)

# tags to numbers

tags_tokenizer = Tokenizer(num_words=len(tags), filters='', oov_token='__UNKNOWN__', lower=False)
tags_tokenizer.fit_on_texts(map(lambda s: ' '.join(s), train_tags))
tag_index = tags_tokenizer.word_index
tag_index['__PADDING__'] = 0
index_tag = {i: w for w, i in tag_index.items()}

index_tag_wo_padding = dict(index_tag)
index_tag_wo_padding[tag_index['__PADDING__']] = '0'

train_tags = tags_tokenizer.texts_to_sequences(map(lambda s: ' '.join(s), train_tags))

train_tags_padded = pad_sequences(train_tags, maxlen=MAX_LEN)

train_tags_padded = np.expand_dims(train_tags_padded, -1)


# build model

random_embedding_layer = Embedding(VOCAB_SIZE, 300, input_length=MAX_LEN)

sequence_input = Input(shape=(MAX_LEN,), dtype='int32')
embedded_sequences = random_embedding_layer(sequence_input)
x = Bidirectional(LSTM(64, return_sequences=True))(embedded_sequences)
x = Dropout(0.3)(x)
x = Dense(32, activation='relu')(x)
preds = Dense(len(tag_index), activation='softmax')(x)

model = Model(sequence_input, preds)
model.compile(loss='sparse_categorical_crossentropy',
              optimizer='adam',
              metrics=['sparse_categorical_accuracy'])

model.summary()

# train model

model.fit(train_sequences_padded[:10], train_tags_padded[:10],
          batch_size=32,
          epochs=10)


